#include <ros/ros.h>
#include "I2CBusController.h"

int main( int argc, char** argv )
{
    ros::init(argc, argv, "raspi_i2c_buscontroller");
    ros::NodeHandle nh;
    ros::NodeHandle nh_private("~");

    raspi_i2c_buscontroller::I2CBusController I2CBusController(nh, nh_private);

    ros::spin();

    return 0;
}

