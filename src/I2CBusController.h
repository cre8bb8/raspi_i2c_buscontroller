#ifndef I2C_BUSCONTROLLER_I2C_BUSCONTROLLER_H
#define I2C_BUSCONTROLLER_I2C_BUSCONTROLLER_H

#include <ros/ros.h>

#include "raspi_i2c_buscontroller/I2CBusAccess.h"
#include "raspi_i2c_buscontroller/I2CMessage.h"

namespace raspi_i2c_buscontroller{

    class I2CBusController {
        public:
            I2CBusController(ros::NodeHandle& nh, ros::NodeHandle& nh_private);

        private:
            ros::NodeHandle nh_;
            ros::NodeHandle nh_private_;

            std::string I2CDeviceName_;

            // write-only access
            ros::Subscriber messageSubscriber_;
            std::string I2CMessageTopic_;
            void I2CMessageCallback(const I2CMessage& message);

            // write-read access
            ros::ServiceServer busAccessServer_;
            std::string I2CBusServiceName_;
            bool busAccessCallback(I2CBusAccess::Request& request,
                    I2CBusAccess::Response& response);


    };
} // Namespace

#endif // include guard

