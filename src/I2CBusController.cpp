#include "I2CBusController.h"

#include <ros/ros.h>

#include <cstdint>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <cstdio>
#include <linux/i2c-dev.h>

namespace raspi_i2c_buscontroller {


I2CBusController::I2CBusController(ros::NodeHandle& nh, ros::NodeHandle& nh_private):
    nh_(nh),
    nh_private_(nh_private)
{
    nh_private_.param("I2C_bus_service_name", I2CBusServiceName_,
        std::string("/i2c_service"));
    nh_private_.param("I2C_message_topic", I2CMessageTopic_,
        std::string("/i2c"));
    nh_private_.param("I2C_device_name", I2CDeviceName_,
        std::string("/dev/i2c-1"));
    
    busAccessServer_ = nh_.advertiseService(I2CBusServiceName_,
            &raspi_i2c_buscontroller::I2CBusController::busAccessCallback,
            this);

    messageSubscriber_ = nh_.subscribe(I2CMessageTopic_, 1,
             &raspi_i2c_buscontroller::I2CBusController::I2CMessageCallback, this);
}

bool I2CBusController::busAccessCallback(I2CBusAccess::Request& request,
        I2CBusAccess::Response& response){

    int I2CFileDescriptor;

    I2CFileDescriptor = open(I2CDeviceName_.c_str(),O_RDWR);
    ioctl(I2CFileDescriptor,I2C_SLAVE,request.message.address);

    if(request.message.mode == I2CMessage::WRITE){
        char writebuf[request.message.data.size()];
        for(uint32_t i = 0; i<request.message.data.size(); i++){
            writebuf[i] = request.message.data[i];
        }
        write(I2CFileDescriptor,writebuf,request.message.data.size());
    }

    char readBuffer[request.readLength];
    if(request.readLength > 0){
        read(I2CFileDescriptor,readBuffer,request.readLength);
        response.data.resize(request.readLength);
        for(uint32_t i = 0; i<request.readLength; i++){
            response.data[i] = readBuffer[i];
        }
        response.success = true;
    }


    close(I2CFileDescriptor);

    return true;
}

void I2CBusController::I2CMessageCallback(const I2CMessage& I2CMsg){

    int I2CFileDescriptor;

    char writebuf[I2CMsg.data.size()];
    for(uint32_t i = 0; i<I2CMsg.data.size(); i++){
        writebuf[i] = I2CMsg.data[i];
    }

    I2CFileDescriptor = open(I2CDeviceName_.c_str(),O_RDONLY);
    ioctl(I2CFileDescriptor,I2C_SLAVE,I2CMsg.address);
    switch(I2CMsg.mode){
        case I2CMessage::WRITE:
            write(I2CFileDescriptor,writebuf,I2CMsg.data.size());
            break;
        case I2CMessage::READ:
            break;
    }

    close(I2CFileDescriptor);

    return;
}

} //namespace
